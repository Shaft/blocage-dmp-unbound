# Eulerian
eulerian.net.
eulerian.fr.
eulerian.com.
et-gv.fr. # Utilisé notamment par le Ministère de l'Intérieur


# Criteo
dnsdelegation.io.
storetail.io.

# AT Internet (feu Xiti)
at-o.net.

# Adobe
omtrdc.net.

# Keyade
keyade.com.

# NP6
bp01.net.

# Webtrekk
webtrekk.com.
webtrekk.net.

# Divers
a.20mn.fr. # Pointe vers un CDN quelconque servant des trackers
